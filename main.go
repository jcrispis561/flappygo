package main

// Imports
import (
	"log"
	"os"
	"time"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
)

// All the game constants
const (
	width          int32  = 800
	height         int32  = 600
	fontPath       string = "res/fonts/game.ttf"
	backgroundPath string = "res/img/background.png"
)

// All loggers
var (
	// WariningLogger logs warning messages
	WarningLogger *log.Logger
	// InfoLogger logs the information messages
	InfoLogger *log.Logger
	// ErrorLogger logs the error messages
	ErrorLogger *log.Logger
)

// Init function for all the loggers
func Init() {
	// Prefix for the InfoLogger
	InfoLogger = log.New(os.Stdout, "[INFO]: ", log.Ldate)
	// Prefix for the WarningLogger
	WarningLogger = log.New(os.Stdout, "[WARNING]: ", log.Ldate)
	// Prefix for the ErrorLogger
	ErrorLogger = log.New(os.Stdout, "[ERROR]: ", log.Ldate)
}

func main() {
	// Entry Point for the game
	setup()
}

func setup() (window *sdl.Window, renderer *sdl.Renderer) {
	err := sdl.Init(sdl.INIT_EVERYTHING)
	if err != nil {
		ErrorLogger.Printf("Cannot Initialize SDL: %v\n", err)
		os.Exit(2)
		return
	}

	InfoLogger.Println("SDL Engine initializated.")
	defer sdl.Quit()
	videoDriver, err := sdl.GetCurrentVideoDriver()
	if err != nil {
		ErrorLogger.Printf("Cannot Get Video Driver Information: %v\n", err)
	}
	InfoLogger.Printf("Using %v Driver", videoDriver)
	currentScreen, err := sdl.GetDisplayName(0)
	if err != nil {
		ErrorLogger.Printf("Cannot Get Display Information")
	}
	InfoLogger.Printf("Shown in display %v", currentScreen)
	window, renderer, err = sdl.CreateWindowAndRenderer(width, height, sdl.WINDOW_OPENGL)
	if err != nil {
		ErrorLogger.Printf("Could not create window and renderer: %v\n", err)
		return
	}
	InfoLogger.Println("Window and Renderer sucessfully created.")
	InfoLogger.Printf("Renderer Using OpenGL Driver\n")
	audioDriver := sdl.GetCurrentAudioDriver()
	InfoLogger.Printf("Using %v audio driver", audioDriver)
	defer window.Destroy()

	drawTitle(renderer)

	time.Sleep(5 * time.Second)
	drawBackground(renderer)
	time.Sleep(5 * time.Second)
	return
}

func drawTitle(renderer *sdl.Renderer) {
	if err := ttf.Init(); err != nil {
		ErrorLogger.Printf("Could not create window and renderer: %v\n", err)
		return
	}
	defer ttf.Quit()
	InfoLogger.Println("ttf Initialized succesfully")
	font, err := ttf.OpenFont(fontPath, 20)
	if err != nil {
		ErrorLogger.Printf("Could not open the font %v: %v\n", fontPath, err)
		return
	}
	defer font.Close()
	InfoLogger.Println("Global Font loaded sucessfully")

	renderer.Clear()
	c := sdl.Color{
		R: 255,
		G: 100,
		B: 0,
		A: 255,
	}
	surface, err := font.RenderUTF8Solid("FlappyGopher", c)
	if err != nil {
		ErrorLogger.Printf("Cannot make the title: %v\n", err)
		return
	}
	defer surface.Free()

	InfoLogger.Println("Title created")
	texture, err := renderer.CreateTextureFromSurface(surface)
	if err != nil {
		ErrorLogger.Printf("Cannot move the surface to GPU: %v\n", err)
	}
	defer texture.Destroy()

	InfoLogger.Printf("Title moved to the GPU.")
	if err := renderer.Copy(texture, nil, nil); err != nil {
		ErrorLogger.Printf("Cannot display title %v \n.", err)
		return
	}
	renderer.Present()
	InfoLogger.Println("Title displayed into the Screen")
}

func drawBackground(renderer *sdl.Renderer) {
	renderer.Clear()
	texture, err := img.LoadTexture(renderer, backgroundPath)
	if err != nil {
		ErrorLogger.Printf("Failed to load background file: %v\n", err)
		return
	}
	InfoLogger.Println("Background moved into the GPU.")
	if err := renderer.Copy(texture, nil, nil); err != nil {
		ErrorLogger.Printf("Cannot Display the background into the GPU: %v\n", err)
		return
	}
	InfoLogger.Println("Background Displayed in the window")
	renderer.Present()
}
